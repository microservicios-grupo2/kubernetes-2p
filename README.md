# Travel Chart

## 73.40 - Arquitectura de Microservicios

### Instituto Tecnológico de Buenos Aires (ITBA)

## Authors

- Sicardi, Julián Nicolas
- Quintairos, Juan Ignacio
- Riera Torraca, Valentino

## Index
- [Authors](#authors)
- [Index](#index)
- [Description](#description)
- [Requirements](#requirements)
- [Installation](#installation)

## Description

This project consists in a chart for Helm containing the Travel Service, consisting of a micro-service solution for managing trips and airports.

## Requirements

- Kubernetes (minikube)
- Helm
- Docker

## Installation

Having fulfilled the requirements mentioned, local directory for volumes should be created according to need. Those path selected should be reflected on the `values.yaml` from the chart.
First you need to start your kubernetes cluster. With minikube, you should first initialize the cluster, binding the local volumes directory with the one choosen inside minikube:

``` bash
    minikube start --mount-string="[HOST_PATH]:/[MINIKUBE_PATH]" --mount
```

Then, you should clone the gitlab repository for the Travel Helm chart, running:

```bash
    git clone https://gitlab.com/microservicios-grupo2/kubernetes-2p.git
```
or (after setting your ssh authentication properly):

```bash
    git clone git@gitlab.com:microservicios-grupo2/kubernetes-2p.git
```
Finally, for installing the helm chart and after completed the `travel/values.yaml`, you should run:

``` bash
    helm install travel travel --values travel/values.yaml
```
By default, the cluster runs using the IP 192.168.49.2

## Configuration values

Travel Chart provides a file `values.yaml`, which serves as configuration for the Travel Service. Values present are:

- Configuration of deployment for airports:
    - `deployments.airports.replicas`: number of replicas for the airports deployment
    - `deployments.airports.maxUnavailable`: maximum number of replicas that can be unavailable during a rolling update
    - `deployments.airports.maxSurge`: maximum number of additional replicas that can be created during a rolling update
    - `deployments.airports.replicas`: number of replicas for the airports deployment
- Configuration of deployment for trips:
    - `deployments.trips.replicas`: number of replicas for the trips deployment
    - `deployments.trips.maxUnavailable`: maximum number of replicas that can be unavailable during a rolling update
    - `deployments.trips.maxSurge`: maximum number of additional replicas that can be created during a rolling update
- Configuration of deployment for gateway:
    - `deployments.gateway.replicas`: number of replicas for the gateway deployment
    - `deployments.gateway.maxUnavailable`: maximum number of replicas that can be unavailable during a rolling update
    - `deployments.gateway.maxSurge`: maximum number of additional replicas that can be created during a rolling update
    - `deployments.gateway.nodePort`: nodePort to assign to the gayeway service
- Configuration of Persistent Volumes for airports:
    - `volumes.aiportPv.storage`: persistent volume storage
    - `volumes.aiportPv.path`: path where the persistent volume is mounted
- Configuration of Persistent Volumes Claims for airports:
    - `pvClaims.aiportPvc.storage`: persistent volume claim storage
- Configuration of Persistent Volumes for trips:
    - `volumes.tripsPv.storage`: persistent volume storage
    - `volumes.tripsPv.path`: path where the persistent volume is mounted
- Configuration of Persistent Volumes Claims for trips:
    - `pvClaims.aiportPvc.storage`: persistent volume claim storage

## Usage

Travel Chart offers a series of endpoints useful for consuming the airports and trips services. For example to ping a service you should make a request similar to:

```bash
    curl http://[CLUSTER_IP]:[NODE_PORT]/trip-ping
```
or

```bash
    curl http://[CLUSTER_IP]:[NODE_PORT]/airport-ping
```

You can request a particular trip or airport by its ID by making a request similar to:

```bash
    curl http://[CLUSTER_IP]:[NODE_PORT]/trip/[TRIP_ID]
```
or

```bash
    curl http://[CLUSTER_IP]:[NODE_PORT]/airport/[AIRPORT_ID]
```

To add new trips or airports, you should make a request similar to:

```bash
    curl --header "Content-Type: application/json" --request POST --data '{"airport_from":"EZE","airport_to":"CPC"}' http://[CLUSTER_IP]:[NODE_PORT]/trip

```
or

```bash
curl --header "Content-Type: application/json" --request POST --data '{"airport": "Ezeiza"}' http://[CLUSTER_IP]:[NODE_PORT]/airport
```

Finally, to do a health check of all services, you can should a request similar to:

```bash
    curl http://[CLUSTER_IP]:[NODE_PORT]/health
```